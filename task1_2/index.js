//  1. Створіть на сторінці div і дайте йому зовнішній
// відступ 150 пікселів.Використовуючи JS виведіть у консоль відступ

let div = document.createElement("div");
document.body.append(div);
div.setAttribute("id", "pad");
div.textContent = "padding"
 console.log( div.style.padding = "150px");


//2.Секундомір

let counter = 1,
    counterMinute = 1,
    counterHours =1,
    counterTime = 0,
    timer;
    const seconds = document.querySelector(".s");
    const minutes = document.querySelector(".m");
    const hours = document.querySelector(".h");
    const start = document.querySelector("#start");
    const stop = document.querySelector("#stop");
    const reset = document.querySelector("#reset");
    
    let secondGo = () => {
      if (counter.toString().length === 1) {
        seconds.innerHTML = `0${counter}`
        counter++
      }
      else if (counter === 60) {
        counter = 0;
        seconds.innerHTML = `0${counter}`
        counter++
        if (counterMinute.toString().length === 1) {
          minutes.innerHTML = `0${counterMinute}`
          counterMinute++
        }
        else if (counterMinute === 60) {
          counterMinute = 0;
          minutes.innerHTML = `0${counterMinute}`
          counterMinute++
        if (counterHours.toString().length === 1) {
            hours.innerHTML = `0${counterHours}`
            counterHours++
          }
          else if (counterHours === 24) {
            counterHours = 0;
            hours.innerHTML = `0${counterMinute}`
            counterHours++
          }
          else {
            hours.innerHTML = counterHours;
            counterHours++
          }
        }
        else {
          minutes.innerHTML = counterMinute
          counterMinute++
        }
      }
      else {
        seconds.innerHTML = counter
        counter++
      };
    };
    
    start.onclick = () => {
      if (counterTime === 0) {
        timer = setInterval(secondGo, 1000);
        counterTime++
      };
    };
    stop.onclick = () => {
      clearInterval(timer);
      counterTime = 0;
    };
    reset.onclick = () => {
      clearInterval(timer);
      seconds.innerHTML = "00";
      minutes.innerHTML = "00";
      hours.innerHTML = "00";
      counter = 1;
      counterMinute = 1;
      counterHours =1;
      counterTime = 0;
    };

    let btn = document.getElementById("start");
    btn.addEventListener("click", function() {
      this.classList.add("active");
    });
    let btn2 = document.getElementById("stop");
    btn2.addEventListener("click", function() {
        this.classList.add("active2");
    });
    let btn3 = document.getElementById("reset");
    btn3.addEventListener("click", function() {
        this.classList.add("active3");
    });