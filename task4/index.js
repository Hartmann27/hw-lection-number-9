const allImages = document.querySelector(".allImages");
const image = document.querySelector("img");
const imagesArray = [
    "http://www.nasa.gov/sites/default/files/thumbnails/image/web_first_images_release.png", 
    "https://spaceplace.nasa.gov/gallery-space/en/NGC2336-galaxy.en.jpg", 
    "https://c.ndtvimg.com/2022-05/dn1f5rjg_galaxy-generic_625x300_20_May_22.jpg?im=Resize=(1230,900)",
    "https://www.cnet.com/a/img/resize/eb43560e9fe1fc534a22160b57142b189f38f686/2022/04/13/f3d51478-28d4-4d52-a9de-833a1e0a7055/low-res-heic2204a-jpg.png?auto=webp&fit=crop&height=675&width=1200",
    "https://cdn.abcotvs.com/dip/images/11882097_052222-wpvi-nasa-universe-expanding-9am-video-vid.jpg?w=1600"
];

let i = 0;
window.setInterval(function slideImage () {
    image.src = imagesArray[i];
    i++;
    if (i == imagesArray.length) {
        i = 0;
    }
}, 3000);